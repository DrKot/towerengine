#ifndef DEVICEAPI_HXX
#define DEVICEAPI_HXX

#include <cstdint>

#include "Device.hxx"

class MessageBus;
class DeviceEnumerator;
class TickCounter;

class DeviceAPI final
{
public:
  DeviceAPI& operator=(const DeviceAPI&) = delete;
  DeviceAPI(const DeviceAPI&) = delete;
  DeviceAPI(const DeviceAPI&&) = delete;
  ~DeviceAPI() = default;
  explicit DeviceAPI(MessageBus* bus, DeviceEnumerator* enumerator, TickCounter* counter);
  uint64_t Ticks() const;
  DeviceType Type(uint32_t id) const;
  bool ReceiveMessage(Device* receiver, uint32_t* dest);
  void SendMessage(Device *sender, uint32_t receiver, uint32_t body, uint32_t ttl);
private:
  MessageBus* _messageBus;
  DeviceEnumerator* _enumerator;
  TickCounter* _counter;
};

#endif