#ifndef RANDOMGENERATOR_HXX
#define RANDOMGENERATOR_HXX
#include <cstdlib>
#include <ctime>
#include "Device.hxx"

class RandomGenerator
{
  friend class DeviceAPI;
public:
  explicit RandomGenerator(uint32_t id) : Device(id)
  {
    srand48(static_cast<uint32_t>(time(nullptr));
  }
  virtual void Update(DeviceAPI*) override
  {

  }
  virtual DeviceType Type() const override
  {
    return DeviceType::RANDOM;
  }
private:
  uint32_t Random() const
  {
    return static_cast<uint32_t>(mrand48());
  }
};
#endif