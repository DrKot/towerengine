#ifndef MESSAGEQUEUE_HXX
#define MESSAGEQUEUE_HXX

class MessageQueue {
private:
  struct Message
  {

  };
public:
  explicit MessageQueue() = default;
  MessageQueue(const MessageQueue&) = delete;
  MessageQueue& operator=(const MessageQueue&) = delete;
  MessageQueue& operator=(const MessageQueue&&) = delete;
  virtual ~MessageQueue()
  {

  }

  void CleanUpExpired(uint64_t)
  {

  }

  void Enqueue(uint32_t , uint32_t , uint32_t , uint64_t )
  {

  }

  bool HasMessages(uint32_t ) const
  {
    return false;
  }

  bool Dequeue(uint32_t , uint32_t* )
  {
    return false;
  }

private:
  // ... <-> _head <-> ... <-> _head <-> ...
};

#endif
