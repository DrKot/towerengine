#ifndef VIRTUALMACHINE_HXX
#define VIRTUALMACHINE_HXX

#include <cstdint>

class MessageBus;
class DeviceEnumerator;
class TickCounter;
class Terminal;
class DeviceAPI;

class VirtualMachine final
{
public:
  explicit VirtualMachine();
  ~VirtualMachine();
  void Update();
  void AddTerminal();

private:
  uint32_t ProduceNewDeviceId()
  {
    return (_nextDeviceId++);
  }
private:
  uint32_t _nextDeviceId = 0;
  MessageBus* _messageBus;
  DeviceEnumerator* _enumerator;
  TickCounter* _counter;
  Terminal* _terminal = nullptr;
  DeviceAPI* _dapi;
};

#endif
