#ifndef DEVICEENUMERATOR_HXX
#define DEVICEENUMERATOR_HXX

#include "Device.hxx"
#include "HashTable.hxx"

class DeviceAPI;

class DeviceEnumerator final : public Device, private HashTable<uint32_t, DeviceType>
{
  friend class DeviceAPI;
public:
  explicit DeviceEnumerator(uint32_t id) : Device(id) {}
  virtual void Update(DeviceAPI*) override
  {

  }
  virtual DeviceType Type() const override
  {
    return DeviceType::ENUMERATOR;
  }
};

#endif