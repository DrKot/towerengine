#include "VirtualMachine.hxx"

#include "TickCounter.hxx"
#include "MessageBus.hxx"
#include "DeviceEnumerator.hxx"
#include "DeviceAPI.hxx"
#include "Terminal.hxx"

VirtualMachine::VirtualMachine()
{
  this->_counter = new TickCounter(ProduceNewDeviceId());
  this->_messageBus = new MessageBus(ProduceNewDeviceId());
  this->_enumerator = new DeviceEnumerator(ProduceNewDeviceId());
  this->_dapi = new DeviceAPI(this->_messageBus, this->_enumerator, this->_counter);
}

VirtualMachine::~VirtualMachine()
{
  delete this->_counter;
  delete this->_messageBus;
  delete this->_enumerator;
  delete this->_terminal;
  delete this->_dapi;
}

void VirtualMachine::Update()
{
  this->_messageBus->Update(this->_dapi);
  this->_enumerator->Update(this->_dapi);
  if (this->_terminal != nullptr) {
    this->_terminal->Update(this->_dapi);
  }
  // This must be the last call:
  this->_counter->Update(this->_dapi);
}

void VirtualMachine::AddTerminal()
{
  if (this->_terminal != nullptr) {
    return;
  }
  this->_terminal = new Terminal(ProduceNewDeviceId());
}

