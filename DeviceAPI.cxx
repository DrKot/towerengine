#include "DeviceAPI.hxx"

#include "Queue.hxx"
#include "MessageBus.hxx"
#include "DeviceEnumerator.hxx"
#include "TickCounter.hxx"

DeviceAPI::DeviceAPI(MessageBus* bus, DeviceEnumerator* enumerator, TickCounter* counter)
  : _messageBus(bus), _enumerator(enumerator), _counter(counter)
{}

uint64_t DeviceAPI::Ticks() const
{
  return _counter->Ticks();
}

DeviceType DeviceAPI::Type(uint32_t id) const
{
  if (DeviceType type; _enumerator->GetKey(id, &type)) {
    return type;
  }
  return DeviceType::UNDEFINED;
}

bool DeviceAPI::ReceiveMessage(Device* receiver, uint32_t* dest)
{
  if (receiver == nullptr || dest == nullptr) {
    return false;
  }
  return this->_messageBus->Dequeue(receiver->ID(), dest);
}

void DeviceAPI::SendMessage(Device *sender, uint32_t receiver, uint32_t body, uint32_t ttl)
{
  if (sender == nullptr) {
    return;
  }
  this->_messageBus->Enqueue(sender->ID(), receiver, body, ttl + this->Ticks());
}
