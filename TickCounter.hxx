#ifndef TICKCOUNTER_HXX
#define TICKCOUNTER_HXX

#include "Device.hxx"

class DeviceAPI;

class TickCounter final : public Device
{
  friend class DeviceAPI;
public:
  explicit TickCounter(uint32_t id) : Device(id) {}
  virtual void Update(DeviceAPI*) override
  {
    ++this->_ticks;
  }
  virtual DeviceType Type() const override
  {
    return DeviceType::TICKER;
  }
private:
  uint64_t Ticks() const
  {
    return _ticks;
  }
  uint64_t _ticks = 0;
};

#endif