#ifndef MESSAGEBUS_HXX
#define MESSAGEBUS_HXX

#include "MessageQueue.hxx"

class Device;
class DeviceAPI;

class MessageBus final : public Device, private MessageQueue
{
  friend class DeviceAPI;
public:
  explicit MessageBus(uint32_t id) : Device(id) {}
  ~MessageBus() = default;
  MessageBus(MessageBus&) = delete;
  MessageBus(MessageBus&&) = delete;
  
  virtual void Update(DeviceAPI*) override
  {
  }
  virtual DeviceType Type() const override
  {
    return DeviceType::BUS;
  }
};

#endif