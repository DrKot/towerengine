#ifndef QUEUE_HXX
#define QUEUE_HXX

#include <cstddef>

template<typename T>
class Queue final
{
public:
  explicit Queue(size_t n) : _head(0), _tail(0), _size(n)
  {
    this->_data = new T[this->_size];
  }

  ~Queue()
  {
    delete [] this->_data;
  }

  Queue(const Queue&) = delete;
  Queue(const Queue&&) = delete;
  Queue& operator=(const Queue&) = delete;

  void Enqueue(T elem)
  {
    if (this->IsFull()) {
      return;
    }
    this->_data[this->_tail] = elem;
    ++this->_tail;
    if (this->_tail == this->_size) {
      this->_tail = 0;
    }
  }

  T Dequeue()
  {
    T res = this->_data[this->_head];
    ++this->_head;
    if (this->_head == this->_size) {
      this->_head = 0;
    }
    return res;
  }

  bool IsFull() const
  {
    if (this->_head == 0 && this->_tail + 1 == this->_size)
      return true;
    if (this->_tail + 1 == this->_head)
      return true;
    return false;
  }

  bool IsEmpty() const
  {
    return this->_tail == this->_head;
  }

  size_t Length() const
  {
    if (this->_tail >= this->_head) {
      return this->_tail - this->_head;
    }
    return this->_size - this->_head + this->_tail;
  }

private:
  size_t _head, _tail;
  size_t _size;
  T* _data;
};
#endif