#include "Terminal.hxx"

#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <stdatomic.h>
#include <unistd.h>

#include "Queue.hxx"
#include "DeviceAPI.hxx"

class AsyncConsole final
{
public:
  bool GetInputIfReady(char* p)
  {
    pthread_mutex_lock(&this->_queueMtx);
    if (!this->_inputReady) {
      pthread_mutex_unlock(&this->_queueMtx);
      return false;
    }
    this->_inputReady = false;
    *p = this->_input;
    pthread_mutex_unlock(&this->_queueMtx);
    return true;
  }
  void ReadChar()
  {
    this->_inputReady = false;
    pthread_mutex_lock(&this->_queueMtx);
    this->_queue.Enqueue({Operation::READ, '\0'});
    pthread_mutex_unlock(&this->_queueMtx);
  }
  void PrintChar(char c)
  {
    pthread_mutex_lock(&this->_queueMtx);
    this->_queue.Enqueue({Operation::WRITE, c});
    pthread_mutex_unlock(&this->_queueMtx);
  }
  ~AsyncConsole()
  {
    pthread_mutex_destroy(&this->_queueMtx);
    // pthread_join(this->_threadID, nullptr);
  }
  void Stop()
  {
    pthread_mutex_lock(&this->_queueMtx);
    this->_queue.Enqueue({Operation::EXIT, '\0'});
    pthread_mutex_unlock(&this->_queueMtx);
    pthread_join(this->_threadID, nullptr);
  }
  explicit AsyncConsole() : _queue(64), _inputReady(false)
  {
    pthread_mutex_init(&this->_queueMtx, nullptr);
    pthread_create(&this->_threadID, nullptr, AsyncConsole::ThreadFunction, this);
  }
private:
  static void* ThreadFunction(void* arg)
  {
    AsyncConsole* obj = static_cast<AsyncConsole*>(arg);
    for (;;) {
      command x;
      pthread_mutex_lock(&obj->_queueMtx);
      if (!obj->_queue.IsEmpty()) {
        x = obj->_queue.Dequeue();
        pthread_mutex_unlock(&obj->_queueMtx);
      }
      else {
        pthread_mutex_unlock(&obj->_queueMtx);
        continue;
      }
      switch (x.op)
      {
        case Operation::READ:
          obj->_input = getchar(); // It is believed this line must be first.
          obj->_inputReady = true;
          break;
        case Operation::WRITE:
          putchar(x.arg);
          break;
        case Operation::EXIT:
          goto pexit;
      }
    }
    pexit:
    pthread_exit(nullptr);
  }
  enum class Operation { READ, WRITE, EXIT };
  struct command
  {
    Operation op;
    char arg;
  };
  Queue<command> _queue;
  bool _inputReady;
  char _input;
  pthread_mutex_t _queueMtx;
  pthread_t _threadID;
};

Terminal::Terminal(uint32_t id)
  : Device(id), _ip(0), _waitingInput(false), _stop(false)
{
  for (int i = 0; i < 16; ++i) this->_r[i] = 0;
  for (int i = 0; i < 256; ++i) this->_code[i] = { opcode::NOP, 0, 0 };
  _cons = new AsyncConsole();
  // Terminal program.
//   this->_code[0] = {opcode::MOVI, '1', 0};
//   this->_code[1] = {opcode::MOVI, '2', 1};
//   this->_code[2] = {opcode::MOVI, '3', 2};
//   this->_code[3] = {opcode::PUTC, 0, 0};
//   this->_code[4] = {opcode::PUTC, 1, 0};
//   this->_code[5] = {opcode::PUTC, 2, 0};
//   this->_code[6] = {opcode::BR, 3, 0};
  this->_code[0] = {opcode::MOVI, '>', 0};
  this->_code[1] = {opcode::MOVI, '\n', 1};
  this->_code[2] = {opcode::MOVI, '=', 2};
  this->_code[3] = {opcode::PUTC, 0, 0};
  this->_code[4] = {opcode::GETC, 4, 0};
  this->_code[5] = {opcode::PUTC, 2, 0};
  this->_code[6] = {opcode::PUTC, 4, 0};
  this->_code[7] = {opcode::PUTC, 1, 0};
  this->_code[8] = {opcode::HLT, 3, 0};
 }

void Terminal::Update(DeviceAPI* api)
{
  if (this->_stop) {
    return;
  }
  if (this->_waitingInput) {
    char x;
    if (this->_cons->GetInputIfReady(&x)) {
      this->_waitingInput = false;
      this->_r[this->_inputRegister] = x;
    }
    return;
  }

  instr i = this->_code[this->_ip];
  uint64_t temp;

  switch (i.op)
  {
    case opcode::GETC:
      this->_inputRegister = i.a & 0xF;
      this->_waitingInput = true;
      this->_cons->ReadChar();
      ++this->_ip;
      break;
    case opcode::CMP:
    case opcode::HLT:
      this->_stop = true;
      this->_cons->Stop();
      break;
    case opcode::NOP:
      ++this->_ip;
      break;
    case opcode::SEND:
      i.a &= 0xF;
      i.b &= 0xF;
      api->SendMessage(this, this->_r[i.a], this->_r[i.b], Terminal::messageTTL);
      ++this->_ip;
      break;
    case opcode::RECV:
      i.a &= 0xF;
      i.b &= 0xF;
      if (api->ReceiveMessage(this, &(this->_r[i.b]))) {
        this->_r[i.a] = 1;
      }
      else {
        this->_r[i.a] = 0;
      }
      ++this->_ip;
      break;
    case opcode::TCKS:
      temp = api->Ticks();
      i.a &= 0xF;
      i.b &= 0xF;
      this->_r[i.a] = static_cast<uint32_t>(0xFFFFFFFF & temp);
      this->_r[i.b] = static_cast<uint32_t>(temp >> 32);
      ++this->_ip;
      break;
    case opcode::BR:
      this->_ip = i.a & 0xFF;
      break;
    case opcode::BRZ:
      i.a &= 0xF;
      i.b &= 0xFF;
      if (this->_r[i.a] == 0) {
        this->_ip = i.b;
      }
      else {
        ++this->_ip;
      }
      break;
    case opcode::BRNZ:
      i.a &= 0xF;
      i.b &= 0xFF;
      if (this->_r[i.a] != 0) {
        this->_ip = i.b;
      }
      else {
        ++this->_ip;
      }
      break;
    case opcode::PUTC:
      _cons->PrintChar(this->_r[i.a & 0xF] & 0xFF);
      ++this->_ip;
      break;
    case opcode::MOV:
      i.a &= 0xF;
      i.b &= 0xF;
      this->_r[i.b] = this->_r[i.a];
      ++this->_ip;
      break;
    case opcode::MOVI:
      i.b &= 0xF;
      this->_r[i.b] = i.a;
      ++this->_ip;
      break;
  }
  this->_ip &= 0xFF;
}