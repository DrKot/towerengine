VM = {
    PC = 0x0, -- program counter
    op_code =
    {
        ["POP"]     = 0, -- POP S removes top element of stack #S
        ["JUMP"]    = 1, -- JUMP A jumps to address A. A is three-byte long.
        ["JUMPZ"]   = 2, -- JUMPZ S1 S2. If top(S1) == 0, then jump to top(S2). Arguments are not consumed.
        ["JUMPNZ"]  = 3, -- JUMPNZ S1 S2. If top(S1) <> 0, then jump to top(S2). Arguments are not consumed.
        ["ECALL"]   = 4, -- ECALL N A R calls `ecall' #N with args in stack #A. -- puts result to stack #R
        ["EBREAK"]  = 5, -- EBREAK N calls debugger function.
        ["PUSH"]    = 6, -- PUSH S 0xABCD puts val 0xABCD to stack #S
        ["LOAD"]    = 7, -- LOAD S 
        ["STORE"]   = 8, -- STORE S
        ["FUN"]     = 9 --- FUN x A R
    },
    funcode_str =
    {
        ["SS"] = 0, -- stack size
        ["CP"] = 1, -- copy
        ["EQ?"] = 2, -- compare
        ["INC"] = 3, -- increment
        ["DEC"] = 4 -- decrement
    },
    stack_size = 0, -- maximum depth of a stack
    stacks = { -- array of stacks
        [0] = {},
        [1] = {},
        [2] = {},
        [3] = {},
        [4] = {},
        [5] = {},
        [6] = {},
        [7] = {}
    },
    memory = {}
}

function VM.fatal_error(msg)
    io.stderr:write(string.format("Fatal system error: %s.\n", msg))
    io.stderr:write("Virtual machine terminated.\n")
    love.event.quit()
    os.exit()
end

function VM.dump_memory(addr, size)
    print(string.format('Debug: dumping memory from %x', addr))
    for i=addr, addr+size-1 do
        print(string.format('%x:    %x', i, VM.memory[i]))
    end
end

function VM.dump_stack(S)
    print(string.format('Debug: dumping stack %d.', S))
    for i=1, #VM.stacks[S] do
        print(string.format('%x', VM.stacks[S][i]))
    end
end

function VM.dump_instruction(addr, op)
    print(string.format('%x:    %s', addr, op.CODE))
end

function VM.decode_instruction(addr)
    local cell = VM.memory[addr]
    if cell == nil then
        VM.fatal_error(string.format('nill cell at 0x%X', addr))
    end

    local op = {}
    op.CODE = cell % 256
    cell = math.floor(cell / 256)
    if op.CODE == VM.op_code.POP then
        op.arg0 = cell % 256
    elseif op.CODE == VM.op_code.JUMP then
        op.arg0 = cell
    elseif op.CODE == VM.op_code.JUMPZ then
        op.arg0 = cell % 256
        cell = math.floor(cell / 256)
        op.arg1 = cell % 256
    elseif op.CODE == VM.op_code.ECALL then
        op.arg0 = cell % 256
        cell = math.floor(cell / 256)
        op.arg1 = cell % 256
        cell = math.floor(cell / 256)
        op.arg2 = cell % 256
    elseif op.CODE == VM.op_code.PUSH then
        op.arg0 = cell % 256
        cell = math.floor(cell / 256)
        op.arg1 = cell
    elseif op.CODE == VM.op_code.LOAD then
        op.arg0 = cell % 256
    elseif op.CODE == VM.op_code.STORE then
        op.arg0 = cell % 256
    elseif op.CODE == VM.op_code.FUN then
        op.arg0 = cell % 256
        cell = math.floor(cell / 256)
        op.arg1 = cell % 256
        cell = math.floor(cell / 256)
        op.arg2 = cell % 256
    else
        VM.fatal_error(string.format('unknown operation code %d', op.CODE))
    end

    return op
end


function VM.assemble(name, arg0, arg1, arg2)
    if name == "POP" then
        return VM.op_code["POP"] + 256 * arg0
    elseif name == "JUMP" then
        return VM.op_code["JUMP"] + 256 * arg0
    elseif name == "JUMPZ" then
        return VM.op_code["JUMPZ"] + 256 * (arg0 + 256 * arg1)
    elseif name == "ECALL" then
        return VM.op_code["ECALL"] + 256 * (arg0 + 256 * (arg1 + 256 * arg2))
    elseif name == "PUSH" then
        return VM.op_code["PUSH"] + 256 * (arg0 + 256 * arg1)
    elseif name == "LOAD" then
        return VM.op_code["LOAD"] + 256 * arg0
    elseif name == "STORE" then
        return VM.op_code["STORE"] + 256 * arg0
    elseif name == "FUN" then
        return VM.op_code["FUN"] + 256 * (VM.funcode_str[arg0] + 256 * (arg1 + 256 * arg2))
    else
        VM.fatal_error(string.format('cannot assemble unknown instruction name %s', name))
    end
end

function VM.initialise(stack_size, memory_size)
    for i = 0, memory_size - 1 do
        VM.memory[i] = 0
    end
    VM.stack_size = stack_size
end

-- Basic VM operations
function VM.op_depth(S)
    return #VM.stacks[S]
end

function VM.op_top(S)
    if #VM.stacks[S] == 0 then
        fatal_error(string.format('stack %d underflow', S))
    end
    return VM.stacks[S][#VM.stacks[S]]
end

function VM.op_pop(S) -- remove top element of stack #S
    if #VM.stacks[S] == 0 then
        fatal_error(string.format('stack %d underflow.', S))
    end
    local a = VM.stacks[S][#VM.stacks[S]]
    VM.stacks[S][#VM.stacks[S]] = nil
    return a
end

function VM.op_push(S, val)
    if #VM.stacks[S] == VM.stack_size then
        VM.fatal_error(string.format('stack %d overflow', S))
    end
    VM.stacks[S][#VM.stacks[S] + 1] = val
end

function VM.op_load(S)
    addr = VM.op_pop(S)
    VM.op_push(S, VM.memory[addr])
end

function VM.op_byte(rA, rB)
   cell = VM.memory[math.floor(VM.reg[rA] / 4)]
   i = VM.reg[rA] % 4
   VM.reg[rB] = cell / (2^i) % (2^(i+1))
end

function VM.op_store(S)
    addr = VM.op_pop(S)
    val = VM.op_pop(S)
    VM.memory[addr] = val
end

function VM.op_jump(addr)
    VM.PC = addr
end

function VM.op_ecall(n, argS, resS)
    if n == 0 then
        print("\nHalt.\n")
        love.event.quit()
    end
    if n == 1 then
        io.stdout:write(string.char(VM.op_pop(argS) % 256))
        VM.op_push(resS, 1)
    end
end

function VM.op_ebreak(n)

end

function VM.op_fun(fun, from, to)
    if fun == VM.funcode_str["SS"] then
        local r = VM.op_depth(from)
        VM.op_push(to, r)
    elseif fun == VM.funcode_str["CP"] then
        local x = VM.stacks[from][#VM.stacks[from]]
        VM.op_push(to, x)
    elseif fun == VM.funcode_str["EQ?"] then
        local x = VM.op_pop(from)
        local y = VM.op_pop(from)
        if x == y then
            VM.op_push(to, 1)
        else
            VM.op_push(to, 0)
        end
    elseif fun == VM.funcode_str["INC"] then
        local x = VM.op_pop(from);
        VM.op_push(to, x + 1)
    elseif fun == VM.funcode_str["DEC"] then
        local x = VM.op_pop(from)
        VM.op_push(to, x - 1)
    else
        VM.fatal_error(string.format('unknown FUN number %d', fun))
    end
end

---------------------------
function VM.draw()
end
---------------------------
function VM.proceed(dt)
    for i=1, 16 do
        local op = VM.decode_instruction(VM.PC)
        if op.CODE == VM.op_code.JUMP then
            VM.op_jump(op.arg0)
            goto next
        end
        if op.CODE == VM.op_code.JUMPZ then
            if VM.op_top(op.arg0) == 0 then
                VM.op_jump(VM.op_top(op.arg1))
            else
                VM.PC = 1 + VM.PC
            end
            goto next
        end
        if op.CODE == VM.op_code.JUMPNZ then
            if VM.op_top(op.arg0) ~= 0 then
                VM.op_jump(VM.op_top(op.arg1))
            else
                VM.PC = 1 + VM.PC
            end
            goto next
        end
        if op.CODE == VM.op_code.PUSH then
            VM.op_push(op.arg0, op.arg1)
        elseif op.CODE == VM.op_code.LOAD then
            VM.op_load(op.arg0)
        elseif op.CODE == VM.op_code.STORE then
            VM.op_store(op.arg0)
        elseif op.CODE == VM.op_code.ECALL then
            VM.op_ecall(op.arg0, op.arg1, op.arg2)
        elseif op.CODE == VM.op_code.POP then
            VM.op_pop(op.arg0)
        elseif op.CODE == VM.op_code.FUN then
            VM.op_fun(op.arg0, op.arg1, op.arg2)
        end
        VM.PC = 1 + VM.PC
        ::next::
    end
end
------------------------------------------------------------
function love.load()
    VM.initialise(64, 0x400)
    VM.memory[0x000] = VM.assemble("JUMP", 0x98)
    VM.memory[0x098] = VM.assemble("PUSH", 2, 5)
    VM.memory[0x099] = VM.assemble("PUSH", 3, 0xA3)
    VM.memory[0x09A] = VM.assemble("PUSH", 1, 0x51)
    VM.memory[0x09B] = VM.assemble("PUSH", 1, 0x201)
    VM.memory[0x09C] = VM.assemble("STORE", 1)
    VM.memory[0x09D] = VM.assemble("PUSH", 0, 0x201)
    VM.memory[0x09E] = VM.assemble("LOAD", 0)
    VM.memory[0x09F] = VM.assemble("ECALL", 1, 0, 0)
    VM.memory[0x0A0] = VM.assemble("FUN", "DEC", 2, 2)
    VM.memory[0x0A1] = VM.assemble("JUMPZ", 2, 3)
    VM.memory[0x0A2] = VM.assemble("JUMP", 0x9A)
    VM.memory[0x0A3] = VM.assemble("ECALL", 0, 0, 0)
end

function love.draw()
    VM.draw()
end

function love.update(dt)
    VM.proceed(dt)
end

function love.quit()
end

