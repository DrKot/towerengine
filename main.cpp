#include <cstdio>
#include <cstdlib>
#include <unistd.h>

#include "VirtualMachine.hxx"
#include "CPU6052.hxx"

int main() {
  VirtualMachine vm;
  vm.AddTerminal();
  CPU6052 c(111);
  for (; ;)  {
    vm.Update();
    c.Update();
  }
  printf("\nVirtual Machine stopped!\n");
  // char t1 = getchar();
  // char t2 = getchar();
  // printf("\n t1 is %d\n t2 is %d\n", static_cast<int>(t1), static_cast<int>(t2));
  return 0;
}
