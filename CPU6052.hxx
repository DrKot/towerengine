#ifndef CPU6502_HXX
#define CPU6502_HXX

#include <cstdint>

#include "Device.hxx"

class CPU6052 : public Device
{
public:
  explicit CPU6052(uint32_t id)
    : Device(id)
  {

    
  }
  virtual void Update(DeviceAPI*) override
  {
    // Fetch
    // Decode
    // Execute
    // Update Program Counter (PC).
  }
  virtual DeviceType Type() const override { return DeviceType::CPU; }
private:
  uint8_t _A, _X, _Y;
  uint8_t _SP;
  uint16_t _PC;
  struct
  {
    unsigned C : 1; // Carry flag.
    unsigned Z : 1; // Zero flag.
    unsigned I : 1; // Interrupts disable.
    unsigned D : 1; // Decimal mode.
    unsigned B : 1; // Break command.
    unsigned O : 1; // Overflow flag.
    unsigned N : 1; // Negative flag.
  } _flags;
};
