#ifndef TERMINAL_HXX
#define TERMINAL_HXX

#include <pthread.h>
#include <cstdint>

#include "Device.hxx"

class DeviceAPI;
class AsyncConsole;

class Terminal final : public Device
{
public:
  explicit Terminal(uint32_t id);
  virtual void Update(DeviceAPI* api) override;
  virtual DeviceType Type() const override
  {
    return DeviceType::TERMINAL;
  }
private:
  enum class opcode
  {
    NOP, // NOP
    TCKS, // TCKS ra rb (ra -- low word, rb -- high word)
    SEND, // SEND ra rb
    RECV, // RECV ra rb (ra - if rb - contains new message)
    BR, // BR ia
    BRZ, // BRZ ra ib
    BRNZ, // BRNZ 
    PUTC, // PUTC ra
    GETC, // GETC ra
    MOV, // MOV ra rb
    MOVI, // MOVI ia rb
    CMP,
    HLT // HLT stops terminal device.
  };
  struct instr
  {
    opcode op;
    uint32_t a, b;
  };

  static constexpr uint32_t messageTTL = 100;
  uint32_t _r[16]; // registers.
  uint32_t _ip; // instruction pointer.
  instr _code[256]; // code memory
  
  AsyncConsole* _cons;
  bool _waitingInput;
  size_t _inputRegister;
  bool _stop;
};
#endif