#ifndef DEVICE_HXX
#define DEVICE_HXX

#include <cstdint>

enum class DeviceType
{
  UNDEFINED,
  RANDOM,
  ENUMERATOR,
  BUS,
  TERMINAL,
  SENSOR,
  TICKER,
  CPU
};

class DeviceAPI;

class Device
{
public:
  explicit Device(uint32_t id) : _id(id) {}
  Device() = delete;
  Device(const Device&) = delete;
  Device& operator=(const Device&) = delete;
  Device& operator=(const Device&&) = delete;
  virtual void Update(DeviceAPI* api) = 0;
  virtual DeviceType Type() const = 0;
  uint32_t ID() const { return this->_id; }

private:
  uint32_t _id;
};


#endif
